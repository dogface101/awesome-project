"""
    COPYRIGHT (C) 2019 by Sebastian Stigler

    NAME
        print_loop.py

    FIRST RELEASE
        2019-03-26  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""


def print_animals():
    """print animals"""
    animals = ['Hund', 'Katze', 'Maus']

    for i, animal in enumerate(animals):
        print("{i}. {animal}".format(i=i+1, animal=animal))


if __name__ == '__main__':
    print_animals()

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
